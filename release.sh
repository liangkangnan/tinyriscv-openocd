#!/bin/bash

set -e

BASE=`pwd`

NPROC=${NPROC:-$((`nproc`*2))}
PARALLEL=${PARALLEL:--j${NPROC}}

echo "relase openocd......"

rm -rf release
mkdir -p release

cp ./src/openocd.exe ./release
cp /mingw32/bin/libconfuse-2.dll ./release
cp /mingw32/bin/libftdi1.dll ./release
cp /mingw32/bin/libftdipp1.dll ./release
cp /mingw32/bin/libgcc_s_dw2-1.dll ./release
cp /mingw32/bin/libhidapi-0.dll ./release
cp /mingw32/bin/libiconv-2.dll ./release
cp /mingw32/bin/libintl-8.dll ./release
cp /mingw32/bin/libstdc++-6.dll ./release
cp /mingw32/bin/libusb-1.0.dll ./release
cp /mingw32/bin/libwinpthread-1.dll ./release
